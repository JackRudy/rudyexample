package com.rudy.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private EditText userName;
    private EditText Password;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();

        String userNameTxt = userName.getText().toString();
        String passWordTxt = Password.getText().toString();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userNameTxt.length()>0&&passWordTxt.length()>0){
                    Utils.showToastMessage(LoginActivity.this,"Login Success!!");


                }
            }
        });
    }

    private void gotoMain(String userNameTxt){
        Intent intentMain = new Intent(LoginActivity.this,MainActivity.class);
        intentMain.putExtra("userLogin",userNameTxt);
        startActivity(intentMain);
        finish();
    }


    private void initView() {
        userName = (EditText) findViewById(R.id.userName);
    }
}