package com.rudy.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.BreakIterator;

public class MainActivity extends AppCompatActivity {

    private Button logOut;
    private LinearLayout btnProfile;
    private BreakIterator userNameMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        String getUserLogin = getIntent().getExtras().getString("userLogin","");

        if(getUserLogin.length()>0){
            userNameMain.setText(getUserLogin);
        }

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent(MainActivity.this,UserDetailActivity.class));
            }
        });


        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


    private void onLogoutClick(){
        startActivity( new Intent(MainActivity.this,LoginActivity.class));
        finish();
    }

    private void initView() {
        logOut = (Button) findViewById(R.id.logOut);
        btnProfile = (LinearLayout) findViewById(R.id.btnProfile);
    }
}