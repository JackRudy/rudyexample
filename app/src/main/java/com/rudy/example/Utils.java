package com.rudy.example;

import android.content.Context;
import android.widget.Toast;

public class Utils {

    public static void showToastMessage(Context context,String Message){
        Toast.makeText(context,Message,Toast.LENGTH_SHORT).show();
    }

}
